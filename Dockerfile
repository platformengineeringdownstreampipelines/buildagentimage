FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y gcc python3-dev python3 python3-pip jq libffi-dev curl tar gzip openssl \
                                    libssl-dev make sudo bash ansible apt-transport-https gnupg2
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
   
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
RUN pip3 install --upgrade pip setuptools
RUN pip3 install ansible[azure]
RUN ansible-galaxy collection install azure.azcollection --force
RUN pip3 install -r https://raw.githubusercontent.com/ansible-collections/azure/dev/requirements-azure.txt
RUN sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
RUN for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sudo sh ./get-docker.sh
ARG AGENT_IMG_VERSION
ENV AGENT_IMG_VERSION=${AGENT_IMG_VERSION}
LABEL version="${AGENT_IMG_VERSION}"
